<?php

if (!defined('ABSPATH')) {
    exit;
}

function cbpw_payment_verify()
{
    global $woocommerce;
    $order_id = sanitize_text_field($_REQUEST['order_id']);
    $nonce = !empty($_REQUEST['nonce']) ? $_REQUEST['nonce'] : "";
    $payment_status_d = !empty($_REQUEST['payment_status']) ? $_REQUEST['payment_status'] : "";
    $order_expired = !empty($_REQUEST['order_expired']) ? sanitize_text_field($_REQUEST['order_expired']) : "";
    $order = new WC_Order($order_id);
    if (!wp_verify_nonce($nonce, 'cbpw-binance-pay')) {
        die("*ok*");
    }

    try {
        $settings_obj = get_option('woocommerce_cbpw_settings');
        $api_key = !empty($settings_obj['cbpw_public_key']) ? $settings_obj['cbpw_public_key'] : "";
        $secret_key = !empty($settings_obj['cbpw_private_key']) ? $settings_obj['cbpw_private_key'] : "";
        $merchant_id = !empty($settings_obj['cbpw_merchantId']) ? $settings_obj['cbpw_merchantId'] : "";
        $sub_merchant_id = !empty($settings_obj['cbpw_sub_merchantId']) ? $settings_obj['cbpw_sub_merchantId'] : "";
        $api = new Cbpw_binance_pay_api();
        $api->cbpw_set_key($api_key, $secret_key);
        $query = array("merchantId" => $merchant_id, "subMerchantId" => $sub_merchant_id, "merchantTradeNo" => $order_id, "prepayId" => null);
        $data = $api->cbpw_query_order($query);
        $payment_status = !empty($data['data']->status) ? $data['data']->status : "";
        $trasn_id = !empty($data['data']->transactionId) ? $data['data']->transactionId : __("issued once the payment is successful", "cbpw");
        // $payment_status="PAID";
        if ($payment_status == "PAID") {
            if ($payment_status_d == "default") {
                $order->add_meta_data('TransectionId', $trasn_id);
                $transection = __('Payment Received via Binance Pay - Transaction ID:', 'cbpw') . $trasn_id;
               $order->add_meta_data('Payment_status', $payment_status);
                $order->add_order_note($transection );
                $order->payment_complete($trasn_id);

            } else {

                $transection = __('Payment Received via Binance Pay - Transaction ID:', 'cbpw') . $trasn_id;
               
                $order->add_meta_data('Payment_status', $payment_status);
                $order->add_order_note($transection );
                $order->update_status(apply_filters('cbpw_capture_payment_order_status', $payment_status_d));
            }
            delete_transient("cbpw_api_data" . $order_id);
        }
        if ($order_expired == "true") {
            $order->add_meta_data('Payment_status', $payment_status);
            $order->add_order_note(__('Your order has been canceled due to over time', 'cbpw'));
            //  $order->add_meta_data('TransectionId', $trasn_id);
            $order->update_status('wc-cancelled', __('Order has been canceled due to Overtime ', 'cbpw'));
        }
        $order->save_meta_data();
        $data = [
            'is_paid' => $order->is_paid(),
            'payment_status' => $payment_status,
            'order_status' => $order->get_status(),
        ];
        echo json_encode($data);
        die();

    } catch (Exception $e) {

    }

    echo json_encode(['status' => 'error', 'error' => 'not a valid order_id']);
    die();
}



   function cbpw_format_number($n)
    {

        if ($n >= 25) {
            return $formatted = number_format($n, 2, '.', ',');
        } else if ($n >= 0.50 && $n < 25) {
            return $formatted = number_format($n, 3, '.', ',');
        } else if ($n >= 0.01 && $n < 0.50) {
            return $formatted = number_format($n, 4, '.', ',');
        } else if ($n >= 0.001 && $n < 0.01) {
            return $formatted = number_format($n, 5, '.', ',');
        } else if ($n >= 0.0001 && $n < 0.001) {
            return $formatted = number_format($n, 6, '.', ',');
        } else {
            return $formatted = number_format($n, 8, '.', ',');
        }
    } 

    //Price conversion API start

 function cbpw_price_conversion($total,$crypto,$type)
    {
        global $woocommerce;

        $currency = get_woocommerce_currency();
        $settings_obj = get_option('woocommerce_cbpw_settings');

        if($type=="cryptocompare"){            
            $api = !empty($settings_obj['cbpw_crypto_compare_key']) ? $settings_obj['cbpw_crypto_compare_key'] : "";
            if(empty($api)){
            return "no_key";
            }
            $current_price = cbpw_crypto_compare_api($currency, $crypto);
            $current_price_array=(array)$current_price;
            

            if(isset($current_price_array['Response'])){
                return ;
            }
            
            $in_crypto = !empty(($current_price_array[$crypto]) * $total) ? ($current_price_array[$crypto]) * $total : "";
            return $in_crypto;
        }
        else{        
        $price_list=cbpw_openexchangerates_api();        
        if(empty($price_list)){
             return "no_key" ;
        }
        $price_arryay=(array)$price_list->rates;
        $current_rate=$price_arryay[$currency];
        
        $binance_price=cbpw_binance_price_api(''.$crypto.'USDT');       
        $lastprice=$binance_price->lastPrice;
        $cal=($total/$current_rate)/$lastprice;
        return  cbpw_format_number($cal);
        }
    }

      function cbpw_crypto_compare_api($fiat, $crypto_token)
    {   
        $settings_obj = get_option('woocommerce_cbpw_settings');

        $api = !empty($settings_obj['cbpw_crypto_compare_key']) ? $settings_obj['cbpw_crypto_compare_key'] : "";
        
        $transient = get_transient("cbpw_currency" . $crypto_token);
        if (empty($transient) || $transient === "") {
            $response = wp_remote_post('https://min-api.cryptocompare.com/data/price?fsym=' . $fiat . '&tsyms=' . $crypto_token . '&api_key='.$api.'', array('timeout' => 120, 'sslverify' => true));
            if (is_wp_error($response)) {
                $error_message = $response->get_error_message();
                return $error_message;
            }
            $body = wp_remote_retrieve_body($response);
            $data_body = json_decode($body);
            set_transient("cbpw_currency" . $crypto_token, $data_body, 10 * MINUTE_IN_SECONDS);
            return $data_body;
        } else {
            return $transient;
        }
    }

     function cbpw_openexchangerates_api()
    {
        $settings_obj = get_option('woocommerce_cbpw_settings');

        $api = !empty($settings_obj['cbpw_openexchangerates_key'])?$settings_obj['cbpw_openexchangerates_key']:"";
        if(empty($api)){
            return;
        }

        $transient = get_transient("cbpw_openexchangerates");
        if (empty($transient) || $transient === "") {
            $response = wp_remote_post('https://openexchangerates.org/api/latest.json?app_id='.$api.'', array('timeout' => 120, 'sslverify' => true));
            if (is_wp_error($response)) {
                $error_message = $response->get_error_message();
                return $error_message;
            }
            $body = wp_remote_retrieve_body($response);
            $data_body = json_decode($body);
            set_transient("cbpw_openexchangerates", $data_body, 120 * MINUTE_IN_SECONDS);
            return $data_body;
        } else {
            return $transient;
        }
    }
     function cbpw_binance_price_api($symbol)
    {
        $transient = get_transient("cbpw_binance_price");
        if (empty($transient) || $transient === "") {
            $response = wp_remote_get('https://api.binance.com/api/v3/ticker/24hr?symbol='.$symbol.'', array('timeout' => 120,'sslverify' => true));
           

            if (is_wp_error($response)) {
                $error_message = $response->get_error_message();
                return $error_message;
            }
            $body = wp_remote_retrieve_body($response);
            $data_body = json_decode($body);
            set_transient("cbpw_binance_price", $data_body, 10 * MINUTE_IN_SECONDS);
            return $data_body;
        } else {
            return $transient;
        }
    }

//Price conversion API end here


   function supported_currency(){
        $oe_currency=array("AED","AFN","ALL","AMD","ANG","AOA","ARS","AUD","AWG","AZN","BAM","BBD","BDT","BGN","BHD","BIF","BMD","BND","BOB","BRL","BSD","BTC","BTN","BWP","BYN","BZD","CAD","CDF","CHF","CLF","CLP","CNH","CNY","COP","CRC","CUC","CUP","CVE","CZK","DJF","DKK","DOP","DZD","EGP","ERN","ETB","EUR","FJD","FKP","GBP","GEL","GGP","GHS","GIP","GMD","GNF","GTQ","GYD","HKD","HNL","HRK","HTG","HUF","IDR","ILS","IMP","INR","IQD","IRR","ISK","JEP","JMD","JOD","JPY","KES","KGS","KHR","KMF","KPW","KRW","KWD","KYD","KZT","LAK","LBP","LKR","LRD","LSL","LYD","MAD","MDL","MGA","MKD","MMK","MNT","MOP","MRO","MRU","MUR","MVR","MWK","MXN","MYR","MZN","NAD","NGN","NIO","NOK","NPR","NZD","OMR","PAB","PEN","PGK","PHP","PKR","PLN","PYG","QAR","RON","RSD","RUB","RWF","SAR","SBD","SCR","SDG","SEK","SGD","SHP","SLL","SOS","SRD","SSP","STD","STN","SVC","SYP","SZL","THB","TJS","TMT","TND","TOP","TRY","TTD","TWD","TZS","UAH","UGX","USD","UYU","UZS","VES","VND","VUV","WST","XAF","XAG","XAU","XCD","XDR","XOF","XPD","XPF","XPT","YER","ZAR","ZMW","ZWL",);
        return $oe_currency;
    }