<?php

class Cbpw_binance_pay_api
{
    /**
     * @var array
     */
   
    public $api_key;
    public $secret_key;
    /**
     * Constructor.
     *
     * @param array $options
     */
    public function __construct()
    {
      
    } 
    public function cbpw_set_key($api,$secret_key){
        $this->api_key = $api;
        $this->secret_key = $secret_key;

    }
    
    public function cbpw_get_public_key(){
        return $this->api_key;
    }
    public function cbpw_get_secret_keys(){        
       return  $this->secret_key ;
    }
  
    //Create new order
    public function cbpw_create_order($req_body){
        $end_point="binancepay/openapi/order";
        return $this->cbpw_request_body($req_body,$end_point);
    }
    
    public function cbpw_query_order($req_body){
        $end_point="binancepay/openapi/order/query";
        return $this->cbpw_request_body($req_body,$end_point);
    }
    public function cbpw_close_order($req_body){
        $end_point = "binancepay/openapi/order/close";

        return $this->cbpw_request_body($req_body,$end_point);
    }
    public function cbpw_transfer_Fund($req_body){
        $end_point = "binancepay/openapi/wallet/transfer";

        return $this->cbpw_request_body($req_body,$end_point);
    }
     public function cbpw_add_submerchant($req_body){
        $end_point = "binancepay/openapi/submerchant/add";

        return $this->cbpw_request_body($req_body,$end_point);
    }
     public function cbpw_refund_order($req_body){
        $end_point = "binancepay/openapi/order/refund";

        return $this->cbpw_request_body($req_body,$end_point);
    }
     public function cbpw_query_refund_prder($req_body){
        $end_point = "binancepay/openapi/order/refund/query";

        return $this->cbpw_request_body($req_body,$end_point);
    }

   

    public function cbpw_request_body($req_body,$end_point){

        $public_key = $this->cbpw_get_public_key();
        $secret_key = $this->cbpw_get_secret_keys();

        if(empty($public_key) || empty($secret_key) ){
        return;
        }
            // Generate nonce string
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $nonce = '';
        for ($i = 1; $i <= 32; $i++) {
        $pos = mt_rand(0, strlen($chars) - 1);
        $char = $chars[$pos];
        $nonce .= $char;
        }       
        //$timestamp = "1611232922428";
        $timestamp = round(microtime(true) * 1000);
        //$nonce = "5RhaTrZPhknNv0kDSA2UQ67cPMVNS4sA";
        // Request body
        //$request = array("merchantId" => "98765987", "subMerchantId" => "98765987", "merchantTradeNo" => rand(), "totalFee" => 25.17, "productDetail" => "Greentea ice cream cone", "currency" => "BUSD", "returnUrl" => "", "tradeType" => "WEB", "productType" => "Food", "productName" => "Ice Cream");
        $json_request = json_encode($req_body);
        $payload = $timestamp . "\n" . $nonce . "\n" . $json_request . "\n";
        $signature = strtoupper(hash_hmac("SHA512", $payload, $secret_key));
       $headers = array();
        $headers["Content-Type"] = "application/json";
        $headers["BinancePay-Timestamp"] = $timestamp;
        $headers["BinancePay-Nonce"] = $nonce;
        $headers["BinancePay-Certificate-SN"] = $public_key;
        $headers["BinancePay-Signature"] = $signature;
        $args = array(
            'method' => 'POST',
            'timeout' => 120,
            'sslverify' => true,
            'headers' => $headers,
            'body' => $json_request,
        );
        $requests_response = wp_remote_post('https://bpay.binanceapi.com/'.$end_point.'', $args);
        
        if (is_wp_error($requests_response)) {
        $error_message = $requests_response->get_error_message();
        return $error_message;
        }
        $body=wp_remote_retrieve_body($requests_response);        
        return (array)json_decode($body);


    }



}
