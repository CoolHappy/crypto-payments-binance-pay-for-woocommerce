<?php
if (!defined('ABSPATH')) {
    exit;
}

class WC_cbpw_Gateway extends WC_Payment_Gateway
{

    /**
     * Class constructor, more about it in Step 3
     */
    public function __construct()
    {

        $this->id = 'cbpw'; // payment gateway plugin ID
        $this->icon = CBPW_URL . '/assets/images/binancecoin.svg'; // URL of the icon that will be displayed on checkout page near your gateway name
        $this->has_fields = true; // in case you need a custom credit card form
        $this->method_title = 'Binance Pay';
        $this->method_description = 'Crypto Payments Binance Pay For Woocommerce'; // will be displayed on the options page
        // gateways can support subscriptions, refunds, saved payment methods,
        // but in this tutorial we begin with simple payments
       
        // Method with all the options fields
        $this->init_form_fields();
        // Load the settings.
        $this->init_settings();
        $this->enabled = $this->get_option('enabled');
        $this->title = $this->get_option('title');
        $this->email_enabled = $this->get_option('email_enabled');
        $this->description = $this->get_option('custom_description');
        $this->default_status = apply_filters('cbpw_process_payment_order_status', 'pending');
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));
       // add_action('admin_enqueue_scripts', array($this, 'payment_scripts'));
        if ( ! $this->is_valid_for_use() ) {
                $this->enabled = 'no';
            }

    }

    public function is_valid_for_use() {
			if ( in_array( get_woocommerce_currency(), apply_filters( 'cbpw_supported_currencies',supported_currency()) ) ) {
				return true;
			}

	    	return false;
        }
    /**
     * Custom CSS and JS
     */
   /*  public function payment_scripts()
    {
        // if our payment gateway is disabled, we do not have to enqueue JS too
        if ('no' === $this->enabled) {
            return;
        }

        wp_enqueue_script('cbpw-custom', CBPW_URL . 'assets/js/cbpw-admin.js', array('jquery'), CBPW_VERSION, true);

    } */

    /**
     * Plugin options, we deal with it in Step 3 too
     */
    public function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title' => 'Enable/Disable',
                'label' => 'Enable Binance Pay',
                'type' => 'checkbox',
                'description' => '',
                'default' => 'yes',
            ),
            'title' => array(
                'title' => __('Title:', 'cbpw'),
                'type' => 'text',
                'description' => __('This controls the title for the payment method the customer sees during checkout.', 'cbpw'),
                'default' => __('Binance Pay', 'cbpw'),
                'desc_tip' => false,
            ),
             'custom_description' => array(
                'title' => 'Description',
                'type' => 'text',
                'description' => 'Add custom description for checkout payment page',
                'default' => 'Select currency',

            ),
            
            'cbpw_merchantId' => array(
                'title' => 'Merchant Id <span style="color:red">(Required *)</span>',
                'type' => 'text',
                'description' => 'The merchant account id, issued when merchant been created at Binance',
                'default' => '',

            ),
            'cbpw_sub_merchantId' => array(
                'title' => 'Sub Merchant Id (Optional)',
                'type' => 'text',
                'description' => 'The sub merchant account id, issued when sub merchant been created at Binance',
                'default' => '',

            ),

            'cbpw_public_key' => array(
                'title' => 'Api Key <span style="color:red">(Required *)</span>',
                'type' => 'text',
                'description' => 'Get your API key<a href="https://developers.binance.com/docs/binance-pay/authentication" target="_blank"> Link</a>',
            ),
            'cbpw_private_key' => array(
                'title' => 'Api Secert Key <span style="color:red">(Required *)</span>',
                'type' => 'text',
                'description' => 'Get your API secret key<a href="https://developers.binance.com/docs/binance-pay/authentication" target="_blank"> Link</a>',
            ),

            'currency_conversion_api' => array(
                'title' => 'Select Currency conversion API',
                'type' => 'select',
                'description' => 'Select currency conversion API for your products',
                'default' => 'openexchangerates',                
                'options' => array(
                    'cryptocompare' => __('Cryptocompare', 'cbpw'),
                    'openexchangerates' => __('Binance + Openexchangerates', 'cbpw'),                   
                ),

            ),

            'cbpw_crypto_compare_key' => array(
                'title' => 'Enter cryptocompare key <span style="color:red">(Required *)</span>',
                'type' => 'text',
                'description' => 'Get your  key<a href="https://min-api.cryptocompare.com/documentation?key=Price&cat=SingleSymbolPriceEndpoint" target="_blank"> Link</a>',
            ),
            'cbpw_openexchangerates_key' => array(
                'title' => 'Enter Openexchangerates key <span style="color:red">(Required *)</span>',
                'type' => 'text',
                'description' => 'Get your  key<a href="https://openexchangerates.org/account/app-ids" target="_blank"> Link</a>',
            ),
            'payment_status' => array(
                'title' => __('Payment Success Status:', 'cbpw'),
                'type' => 'select',
                'description' => __('Payment action on successful Transaction ID submission.', 'cbpw'),
                'desc_tip' => false,
                'default' => 'default',
                'options' => apply_filters('cbpw_settings_order_statuses', array(
                    'default' => __('Woocommerce Default Status', 'cbpw'),
                    'on-hold' => __('On Hold', 'cbpw'),
                    'processing' => __('Processing', 'cbpw'),
                    'completed' => __('Completed', 'cbpw'),
                )),
            ),

            'select_currency' => array(
                'title' => 'Select Crypto Currency',
                'type' => 'select',
                'description' => 'Select crypto currency to show for payment',
                'default' => 'BUSD',
                'options' => array(
                    'BUSD' => __('BUSD', 'cbpw'),
                    'BTC' => __('BTC (Coming soon..)', 'cbpw'),
                    'TETHER' => __('TETHER (Coming soon..)', 'cbpw'),
                ),

            ),
            'expiry_time' => array(
                'title' => 'Select Order Expiry Time',
                'type' => 'select',
                'description' => 'Select Expiry time for your product',
                'default' => '45',
                'options' => array(
                    '50' => __('10 Minutes', 'cbpw'),
                    '45' => __('15 Minutes', 'cbpw'),
                    '40' => __('20 Minutes', 'cbpw'),
                    '35' => __('25 Minutes', 'cbpw'),
                    '30' => __('30 Minutes', 'cbpw'),
                    '25' => __('35 Minutes', 'cbpw'),
                    '20' => __('40 Minutes', 'cbpw'),
                ),

            ),
           
            'payement_msg' => array(
                'title' => 'Payment Success Message ',
                'type' => 'text',
                'description' => 'Add custom message for successfull payment',
                'default' => 'Your payment has been received, order will be processed by Author',

            ),
            'order_cncl_msg' => array(
                'title' => 'Order expired message ',
                'type' => 'text',
                'description' => 'Add custom message for expired order ',
                'default' => 'Your order has been expired',

            ),
            

        );

    }

    public function payment_fields()
    {
        wp_enqueue_style('ca-loader-css', CBPW_URL . 'assets/css/cbpw.css');
        $crypto_currency = $this->get_option("select_currency");        
        $type=$this->get_option('currency_conversion_api');  
        $total_price = $this->get_order_total();        
        $in_busd = cbpw_price_conversion($total_price,"BUSD",$type);          
        do_action('woocommerce_cbpw_form_start', $this->id);
        if(!empty($in_busd) && $in_busd!="no_key"){
        ?>
                <div class="form-row form-row-wide">
                    <p><?php echo $this->description; ?></p>
                    <div class="cbpw-pymentfield">
                    <input id="cbpw_payment_method" type="radio" class="input-radio" name="cbpw_crypto_coin" value="<?php echo !empty($in_busd)?esc_attr($crypto_currency):""; ?>"/>
                        <img src="<?php echo CBPW_URL . '/assets/images/binancecoin.svg'; ?>"/>
                        <span><?php echo esc_html($crypto_currency) ?></span>
                    <p class="cbpw_crypto_price"><?php echo esc_html($in_busd . $crypto_currency) ?></p>
                    </div>
                   
               </div>
                <?php
        }
        elseif ($in_busd=="no_key") {
             echo __('<strong>Please enter Your price conversion key</strong>', 'cbpw') 

            ?>             
              <input id="cbpw_no_key" type="hidden"  name="cbpw_no_key" value="<?php echo esc_attr($in_busd); ?>"/>
            <?php

        }
        else{
             echo __('<strong>Currency is not supported</strong>', 'cbpw') 
            ?>             
              <input id="cbpw_currency_not_supported" type="hidden"  name="cbpw_currency_not_supported" value="<?php echo esc_attr($in_busd); ?>"/>
            <?php

        }
        do_action('woocommerce_cbpw_form_end', $this->id);
    }

    public function validate_fields()
    {

        $api_key = $this->get_option('cbpw_public_key');
        $secret_key = $this->get_option('cbpw_private_key');
        if(empty($api_key ) || empty($secret_key ) ){
              wc_add_notice(__('<strong>Please enter your Binance pay API key</strong>', 'cbpw'), 'error');
            return false;

        }
        if(isset($_POST['cbpw_currency_not_supported'])){
             wc_add_notice(__('<strong>Currency is not supported</strong>', 'cbpw'), 'error');
            return false;
            
        }
        if (isset($_POST['cbpw_no_key']) && $_POST['cbpw_no_key']=="no_key") {
            wc_add_notice(__('<strong>Please enter Your price conversion key</strong>', 'cbpw'), 'error');
            return false;
        }
        if (empty($_POST['cbpw_crypto_coin'])) {
            wc_add_notice(__('<strong>Select a Currency for Process Payment </strong>', 'cbpw'), 'error');
            return false;
        }
        return true;
    }

    public function process_payment($order_id)
    {
        global $woocommerce;

        try {
            $order = new WC_Order($order_id);
            $crypto_currency = !empty($_POST['cbpw_crypto_coin']) ? sanitize_text_field($_POST['cbpw_crypto_coin']) : '';
            $total = $order->get_total();
            $type=$this->get_option('currency_conversion_api');            
            $in_crypto = cbpw_price_conversion($total,$crypto_currency,$type);
            $api_key = $this->get_option('cbpw_public_key');
            $secret_key = $this->get_option('cbpw_private_key');
            $merchant_id = $this->get_option('cbpw_merchantId');
            $sub_merchant_id = $this->get_option('cbpw_sub_merchantId');
            $items = $order->get_items();
            $product_name = "";
            $product_type = "";
            $product_desc = "";

            foreach ($items as $item) {
                $product = $item->get_product();
                // Now you have access to (see above)...
                $product_name = !empty($product->get_name()) ? $product->get_name() : "";
                $product_type = !empty($product->get_type()) ? $product->get_type() : "";
                $product_desc = !empty($product->get_short_description()) ? $product->get_short_description() : "";
            }
            $request = array("merchantId" => $merchant_id, "subMerchantId" => $sub_merchant_id, "merchantTradeNo" => $order_id, "totalFee" => $in_crypto, "productDetail" => $product_desc, "currency" => "BUSD", "returnUrl" => "", "tradeType" => "WEB", "productType" => $product_type, "productName" => $product_name);
            $tras_name = "cbpw_api_data" . $order_id;
            $traans = get_transient($tras_name);
            $qyrlink = "";
            $timestamp = "";
            $raw = "";
            $api = new Cbpw_binance_pay_api();
            $api->cbpw_set_key($api_key, $secret_key);
            if (empty($traans) || $traans === "") {
                $qr_data = $api->cbpw_create_order($request);
                if($qr_data['status']=="FAIL"){
                    return;
                }
                $qyrlink = $qr_data['data']->qrcodeLink;
                $timestamp = $qr_data['data']->expireTime;
                $raw = $qr_data;
                set_transient($tras_name, $qr_data, 1 * HOUR_IN_SECONDS);
            } else {
                $qyrlink = $traans['data']->qrcodeLink;
                $timestamp = $traans['data']->expireTime;
                $raw = $traans;
            }
            $crypto_total = $total;
            $order->add_meta_data('cbpw_timestamp', $timestamp);
            $order->add_meta_data('cbpw_total', $crypto_total);
            $order->add_meta_data('cbpw_qr_code', $qyrlink);
            $order->add_meta_data('cbpw_in_crypto', $in_crypto);
            $order->add_meta_data('cbpw_product_type', $raw);
            $order->save_meta_data();
            $order->update_status($this->default_status);
            $woocommerce->cart->empty_cart();
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url($order),
            );

        } catch (Exception $e) {
            wc_add_notice(__('Payment error:', 'cbpw') . 'Unknown coin', 'error');
            return null;
        }
        wc_add_notice(__('Payment error:', 'woocommerce') . __('Payment could not be processed, please try again', 'cbpw'), 'error');
        return null;
    }

    public function thankyou_page($order_id)
    {
        $payment_msg = !empty($this->get_option('payement_msg'))?$this->get_option('payement_msg'):"Your payment has been received, order will be processed by Author";
        $expirytime = !empty($this->get_option('expiry_time'))?$this->get_option('expiry_time'):"45";
        $order_expire_msg = !empty($this->get_option('order_cncl_msg'))?$this->get_option('order_cncl_msg'):"Your order has been expired";
        $order = new WC_Order($order_id);
        $total = $order->get_total();
        $nonce = wp_create_nonce('cbpw-binance-pay');
        $crypto_value = $order->get_meta('cbpw_total');
        $qr_code_img = $order->get_meta('cbpw_qr_code');
        
        $timestamp = $order->get_meta('cbpw_timestamp');
        $decrease_time = !empty($timestamp)?($timestamp - $expirytime * 60 * 1000):"";
        $in_crypto = $order->get_meta('cbpw_in_crypto');
        $payment_status= $order->get_meta('Payment_status');

        wp_enqueue_script('cbpw-timer', CBPW_URL . 'assets/js/cbpw-custom.js', array('jquery'), CBPW_VERSION, true);
        wp_localize_script('cbpw-timer', "obj", array('expiry_redirect' => home_url('/shop'), 'timestamp' => $decrease_time, 'ajax' => home_url('/wp-admin/admin-ajax.php'), 'id' => $order_id, 'order_status' => $payment_status, 'nonce' => $nonce, 'payment_status' => $this->get_option('payment_status'),
        ));
        wp_enqueue_style('cbpw_custom_css', CBPW_URL . 'assets/css/cbpw.css');        
        ?>
        <div class="cbpw-expire-order"></div>
        <div class="cbpw_payment-panel">
            <div class="cbpw_loader">
                <div>
                    <div class="lds-css ng-scope">
                        <div class="cbpw-lds-dual-ring">
                            <div></div>
                            <div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cbpw_check">
                <img src="<?php echo esc_url(CBPW_URL . '/assets/images/binancecoin.svg') ?>"/>
            </div>
            <div class="cbpw_payment_details">
                <h4><?php echo __('Waiting for payment', 'cbpw') ?></h4>
                <div class="cbpw_qrcode_wrapper">
                    <div class="cbpw_inner-wrapper">
                            <img src="<?php echo esc_url($qr_code_img); ?>" alt="Loading..."/>
                    </div>
                    <span class="cbpw_banner"><h2><?php echo __('ONLY PAY WITH', 'cbpw');?></h2><img src="<?php echo esc_url(CBPW_URL . '/assets/images/binance-pay.svg') ?>" alt="Loading..."/></span>
                </div>
                <div class="cbpw_details_box">
                    <div class="cbpw-timer"></div>
                    <?php echo __('Amount to pay:', 'cbpw') ?>
                    <span><b><?php echo esc_html($in_crypto) . ' BUSD' ?></b></span>
                    <?php echo __('Scan the QR in the Binance App!', 'cbpw') ?>
                </div>
            </div>
            <div class="cbpw_payment_pending">
                <h4><?php echo esc_html($payment_msg) ?></h4>
            </div>
            <div class="cbpw_payment_complete">
                <h4><?php echo esc_html($payment_msg) ?></h4>
            </div>
            <div class="cbpw_order_expired">
                <h4><?php echo esc_html($order_expire_msg) ?></h4>
            </div>
        </div>
        <?php

    }
   
  

   


}
