var coin_list = jQuery('#woocommerce_cbpw_select_currency option');
jQuery(coin_list[1]).prop('disabled', 'disabled');
jQuery(coin_list[2]).prop('disabled', 'disabled');


var choose_api = jQuery('#woocommerce_cbpw_currency_conversion_api');

if (jQuery(choose_api).val() == "cryptocompare") {
    jQuery('#woocommerce_cbpw_cbpw_crypto_compare_key').parents('tr').show();
    jQuery('#woocommerce_cbpw_cbpw_openexchangerates_key').parents('tr').hide();
} else {
    jQuery('#woocommerce_cbpw_cbpw_crypto_compare_key').parents('tr').hide();
    jQuery('#woocommerce_cbpw_cbpw_openexchangerates_key').parents('tr').show();
}
 choose_api.on('change', function () {
     if (jQuery(this).val() == "cryptocompare") {
         jQuery('#woocommerce_cbpw_cbpw_crypto_compare_key').parents('tr').show();
         jQuery('#woocommerce_cbpw_cbpw_openexchangerates_key').parents('tr').hide();
     } else {
         jQuery('#woocommerce_cbpw_cbpw_crypto_compare_key').parents('tr').hide();
         jQuery('#woocommerce_cbpw_cbpw_openexchangerates_key').parents('tr').show();
     }
 });



jQuery('.cbpw_modal-toggle').on('click', function (e) {
    e.preventDefault();
    jQuery('.cbpw_modal').toggleClass('is-visible');
    var iBody = jQuery("#cbpw_custom_cbpw_modal").contents().find("body");
    //  var updt_trgt = jQuery(iBody).find('#plugin-information-footer a').attr('target', '_blank');
    var trget_link = jQuery(iBody).find('#plugin-information-footer a').attr('href');
    // var chklink = (trget_link == "undefined") ?"Latest Version Installed":"";
    var adddat = jQuery(iBody).find('#plugin-information-footer').html("<a data-slug='woocommerce' id='plugin_install_from_iframe' class='button button-primary right' href=" + trget_link + " target='_blank'>Install Now</a>")
    // console.log(trget_link)


});
