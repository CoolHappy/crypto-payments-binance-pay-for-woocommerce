

var timestamp = (obj.timestamp) - Date.now();
let is_paid = false;
var order_expired=false;
console.log(obj.order_status)
if (obj.order_status!="PAID"){
status_loop();
var status_loop_id= setInterval(status_loop, 10000);

timestamp /= 1000; // from ms to seconds

function component(x, v) {
    return Math.floor(x / v);
}

var $div = jQuery('.cbpw-timer');

var timeid=setInterval(function () {
    timestamp--;
    var days = component(timestamp, 24 * 60 * 60),
        hours = component(timestamp, 60 * 60) % 24,
        minutes = component(timestamp, 60) % 60,
        seconds = component(timestamp, 1) % 60;
   
    if (minutes <= 0 && seconds <= 0 ){
       order_expired = true;      
        clearInterval(timeid);        
    }

    $div.html("Your order will expire in <h4>"+  minutes + ":" + seconds +" min</h4>");
}, 1000);

           
    function status_loop() {        
        var request_data = {
            'action': 'cbpw_order_status',
            'nonce': obj.nonce,
            'order_id': obj.id,
            'order_expired':order_expired,
            'payment_status': obj.payment_status,
        };
        jQuery.ajax({
            type: "post",
            dataType: "json",
            url: obj.ajax,
            data: request_data,
            success: function (data) {                
                if (data.payment_status=='PAID') {
                    jQuery('.cbpw_payment_details,.cbpw_payment_complete,.cbpw_loader').hide(200);
                    jQuery('.cbpw_payment_pending,.cbpw_check').show(200);
                    clearInterval(status_loop_id);                   
                    location.reload();
                 }

                if (data.is_paid==true) {                   
                    jQuery('.cbpw_loader,.cbpw_payment_pending,.cbpw_payment_details').hide(200);
                    jQuery('.cbpw_payment_complete,.cbpw_check').show(200);
                    is_paid = true;
                    clearInterval(status_loop_id);                 
                    location.reload();
                 
                }
                
                if (data.order_status == "cancelled" || order_expired==true) {
                    jQuery('.cbpw_loader,.cbpw_payment_pending,.cbpw_payment_details').hide(200);
                    jQuery('.cbpw_order_expired,.cbpw_check').show(200);                    
                    clearInterval(status_loop_id);                  

                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("Status: " + textStatus + "Error: " + errorThrown);
            }
        })
       

        
    }

}
else{
    jQuery('.cbpw_loader,.cbpw_payment_pending,.cbpw_payment_details').hide(200);
    jQuery('.cbpw_payment_complete,.cbpw_check').show(200);
}
